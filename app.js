const express = require('express');
const path = require('path');
const app = express();

const publicPath = path.resolve(__dirname, 'public');
app.use(express.static(publicPath));
app.set('view engine', 'hbs');

const KindredAPI = require('kindred-api');
const REGIONS = KindredAPI.REGIONS;
const QUEUES = KindredAPI.QUEUE_TYPES;
const debug = true;
const k = KindredAPI.QuickStart('RGAPI-dbdfb1b4-3a82-499b-b914-7388659e866e', REGIONS.NORTH_AMERICA, debug);
const Q_STRINGS = KindredAPI.QUEUE_STRINGS;

const name = 'MalusPuer';
const region = REGIONS.NORTH_AMERICA
const options = {
    // no need for joins or hardcoded numbers
    queue: [QUEUES.TEAM_BUILDER_RANKED_SOLO, QUEUES.RANKED_FLEX_SR],
    // array values will always be joined into a string
    champion: 79
    // option params should be spelled and capitalized the same as it is in Riot's docs!
    // ex: Matchlist params in Riot's docs include `champion`, `beginIndex`, `beginTime`, `season`
}

k.Matchlist.get({ name }, KindredAPI.print) // full matchlist


app.get('/', (req, res) => {
    res.render('index');
});

app.listen(3000);
